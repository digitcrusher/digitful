# digitful

A simple 2D lighting simulation in OpenGL 4.3 written in C.

![A screenshot](screenshot.png)

## Building and running

First you have to clone the repository recursively so that the CImGui sources in `external/cimgui/` are downloaded.

    git clone --recursive https://gitlab.com/digitcrusher/digitful.git
    cd digitful/

Make sure you have the development package for GLFW 3 installed on your system and then run the build script provided in the project's top directory. You also need Python 3 for this one.

    ./build.py

It may take a while when running the first time, especially when compiling resources into the executable (more on that later), but when you run the build script any number of times after that, only the files that have been modified will be processed again thanks to a custom incremental compilation solution. That's one of the things Python is in here for. *The other thing is that I don't like shell scripts (including Makefiles) and the limitations of using build systems.*

If you want to build an executable with all the resources already in it without the need for an external `res/` folder, you can change the value of the variable `should_compile_tests_reses` in build.py to `True`. With that enabled, you can still replace the executable's resources with your own by placing them in a directory named `res` in the executable's current working directory. If you want to cross-compile for Windows (on Linux), set the variable `should_compile_for_win` to `True`. The script assumes by default that you have GCC (or MinGW for cross-compiling to Windows) installed but you can use another compiler by changing the value of the variable `cc` at the beginning of the script and swapping the flags in `flags`, `external_flags` and `test_flags` to their equivalents in your preferred compiler if needed.

Having run the build script, you should now have an executable file named "digitful" in `build/test/` ready for you to run.

    cd build/test/
    ./digitful

Inside the application you can press the Escape key to toggle a window with a list of options for the scene. There are some presets in there that I have set up but the rest is for you to play around with.

## How does it work?

The basis of the simulation are two light maps - a primary one and a secondary one, each of which holds the illumination levels of the red, green and blue color components of every pixel on the screen in an unclamped floating-point framebuffer.

- At first, all the pixels in the light maps are zero/black. Then one of the lights in the scene is cast in the secondary light map effectively filling it with an inverse quadratically interpolated radial gradient from the light's color to black.
- For every light obstacle in the scene, the secondary light map is copied into a temporary texture and the ends of each of the obstacle's line segments are extended out from the light source by a *big enough* factor forming a shadow in the form of a quadrilateral. These quads are then modified to add a one pixel margin around them and rendered in the secondary light map, in such a way that they look like a view into the temporary texture whose pixels' colors have been multiplied by the obstacle's color. The CPU only generates some rough geometry for the fragment shader, where the real magic happens. It is the part that actually decides which pixels are inside the shadow and which are not. I've found this method of off-loading the work to the fragment shader to be the only one allowing precise anti-aliasing.
- Having processed all the light obstacles like that, the secondary light map is added onto the main one and cleared to black.

This process is repeated for all lights in the scene and after all that, the main light map is multiplied onto the 0-1 clamped default framebuffer, which was filled with a solid color or an image beforehand.

There are some technical details, such as gamma correction, shadow anti-aliasing or fixing color banding with dithering, but you can learn about those by just reading the code. 😉 You may also wonder why I rolled my own shadow anti-aliasing instead of using some ready-made technique such as MSAA. Well, OpenGL's built-in multisampling was just too *slow* for my liking.

Of course, this demo is not much more than a proof of concept. There are other things that you could add to this, such as soft shadows or directional lights, but I wanted to keep this little project of mine simple.
