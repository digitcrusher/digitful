/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/gl_utils.h>
#include <stb_image.h>
#include <digitful/gres.h>
#include <digitful/utils.h>

Shader *simple_tex_shader;
GLuint overlay_vbo, tex_overlay_vbo;
GLuint overlay_vao, tex_overlay_vao;

static bool is_inited = false;

void gl_utils_init() {
  if(is_inited) return;
  is_inited = true;

  puts("Creating simple_tex shader...");
  simple_tex_shader = shader_new_from_gres("digitful/simple_tex.vert", "digitful/simple_tex.frag");
  if(simple_tex_shader == NULL) {
    exit(EXIT_FAILURE);
  }

  glGenBuffers(1, &overlay_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, overlay_vbo);
  float overlay_vertices[] = {
    -1.0, -1.0,
     1.0, -1.0,
     1.0,  1.0,
    -1.0,  1.0,
  };
  glBufferData(GL_ARRAY_BUFFER, sizeof(overlay_vertices), overlay_vertices, GL_STATIC_DRAW);

  glGenVertexArrays(1, &overlay_vao);
  glBindVertexArray(overlay_vao);
  VertexAttribute overlay_attrs[] = {{GL_FLOAT, 2}};
  set_vao_attributes(overlay_attrs, array_size(overlay_attrs));

  glGenBuffers(1, &tex_overlay_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, tex_overlay_vbo);
  float tex_overlay_vertices[] = {
    -1.0, -1.0, 0.0, 0.0,
     1.0, -1.0, 1.0, 0.0,
     1.0,  1.0, 1.0, 1.0,
    -1.0,  1.0, 0.0, 1.0,
  };
  glBufferData(GL_ARRAY_BUFFER, sizeof(tex_overlay_vertices), tex_overlay_vertices, GL_STATIC_DRAW);

  glGenVertexArrays(1, &tex_overlay_vao);
  glBindVertexArray(tex_overlay_vao);
  VertexAttribute tex_overlay_attrs[] = {{GL_FLOAT, 2}, {GL_FLOAT, 2}};
  set_vao_attributes(tex_overlay_attrs, array_size(tex_overlay_attrs));
}

void gl_utils_deinit() {
  if(!is_inited) return;
  is_inited = false;

  glDeleteBuffers(1, &overlay_vbo);
  glDeleteVertexArrays(1, &overlay_vao);

  glDeleteBuffers(1, &tex_overlay_vbo);
  glDeleteVertexArrays(1, &tex_overlay_vao);

  puts("Destroying simple_tex shader...");
  shader_destroy(simple_tex_shader);
}



GLuint tex2d_new(int width, int height, GLenum format, GLenum min_filter, GLenum mag_filter) {
  GLuint result;
  glGenTextures(1, &result);
  glBindTexture(GL_TEXTURE_2D, result);
  glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, GL_RGB, GL_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
  return result;
}

GLuint tex2d_new_from_data(int width, int height, GLenum format, void *data, GLenum data_format, GLenum data_type, GLenum min_filter, GLenum mag_filter) {
  GLuint result;
  glGenTextures(1, &result);
  glBindTexture(GL_TEXTURE_2D, result);
  glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, data_format, data_type, data);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
  return result;
}

GLuint tex2d_new_from_file(const char *filename, GLenum min_filter, GLenum mag_filter) {
  stbi_set_flip_vertically_on_load(true);

  int width, height;
  unsigned char *data = stbi_load(filename, &width, &height, NULL, 4);
  if(data == NULL) {
    printf("Failed to load image '%s'\n", filename);
    stbi_image_free(data);
    return 0;
  }

  GLuint result = tex2d_new_from_data(width, height, GL_RGBA, data, GL_RGBA, GL_UNSIGNED_BYTE, min_filter, mag_filter);
  glGenerateMipmap(GL_TEXTURE_2D);

  stbi_image_free(data);
  return result;
}

GLuint tex2d_new_from_mem(void *img_data, size_t img_size, GLenum min_filter, GLenum mag_filter) {
  stbi_set_flip_vertically_on_load(true);

  int width, height;
  unsigned char *data = stbi_load_from_memory(img_data, img_size, &width, &height, NULL, 4);
  if(data == NULL) {
    printf("Failed to load image from memory at %p\n", img_data);
    stbi_image_free(data);
    return 0;
  }

  GLuint result = tex2d_new_from_data(width, height, GL_RGBA, data, GL_RGBA, GL_UNSIGNED_BYTE, min_filter, mag_filter);
  glGenerateMipmap(GL_TEXTURE_2D);

  stbi_image_free(data);
  return result;
}

GLuint tex2d_new_from_gres(const char *name, GLenum min_filter, GLenum mag_filter) {
  Resource *res = gres_get(name);
  if(res == NULL) {
    return 0;
  }
  GLuint result = tex2d_new_from_mem(res->data, res->size, min_filter, mag_filter);
  gres_release(res);
  return result;
}

GLuint tex2dms_new(int width, int height, GLenum format, int samplec) {
  GLuint result;
  glGenTextures(1, &result);
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, result);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samplec, format, width, height, true);
  return result;
}



static size_t gl_type_size(GLenum type) {
  switch(type) {
  case GL_FLOAT:
    return sizeof(float);
  default:
    printf("Unknown gl type: %i\n", type);
    return -1;
  }
}

void set_vao_attributes(VertexAttribute attrs[], size_t attrc) {
  size_t stride = 0;
  for(size_t i = 0; i < attrc; i++) {
    stride += attrs[i].count * gl_type_size(attrs[i].type);
  }

  size_t offset = 0;
  for(size_t i = 0; i < attrc; i++) {
    glVertexAttribPointer(i, attrs[i].count, attrs[i].type, GL_FALSE, stride, (void*) offset);
    glEnableVertexAttribArray(i);
    offset += attrs[i].count * gl_type_size(attrs[i].type);
  }
}
