/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <digitful/context.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <digitful/utils.h>

void context_init(Context *self) {
  self->state_stack = vec_new(ContextState);
  ContextState state = {
    .draw_fbo = 0,
    .read_fbo = 0,
    .shader = NULL,
    .viewport = {
      .x = 0, .y = 0, .w = 1, .h = 1
    }
  };
  vec_append(self->state_stack, state);
}

void context_deinit(Context *self) {
  vec_destroy(self->state_stack);
}



Context *curr_ctx = NULL;

ContextState *ctx_state() {
  return &vec_last(curr_ctx->state_stack);
}

void ctx_push() {
  vec_append(curr_ctx->state_stack, *ctx_state());
}

void ctx_pop() {
  if(vec_size(curr_ctx->state_stack) == 0) return;
  vec_pop(curr_ctx->state_stack);

  ContextState *state = ctx_state();

  if(state->draw_fbo == state->read_fbo) {
    glBindFramebuffer(GL_FRAMEBUFFER, state->draw_fbo);
  } else {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, state->draw_fbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, state->read_fbo);
  }
  if(state->shader != NULL) {
    glUseProgram(state->shader->id);
  }
  glViewport(state->viewport.x, state->viewport.y, state->viewport.w, state->viewport.h);
}



void ctx_bind_fbo(GLuint fbo) {
  ctx_state()->draw_fbo = fbo;
  ctx_state()->read_fbo = fbo;
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void ctx_bind_draw_fbo(GLuint fbo) {
  ctx_state()->draw_fbo = fbo;
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
}

void ctx_bind_read_fbo(GLuint fbo) {
  ctx_state()->read_fbo = fbo;
  glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
}



void ctx_use_shader(Shader *shader) {
  ctx_state()->shader = shader;
  if(shader != NULL) {
    glUseProgram(shader->id);
  }
}

void ctx_set_uniform_1f(const char *name, float a) {
  glUniform1f(glGetUniformLocation(ctx_state()->shader->id, name), a);
}

void ctx_set_uniform_2f(const char *name, float a, float b) {
  glUniform2f(glGetUniformLocation(ctx_state()->shader->id, name), a, b);
}

void ctx_set_uniform_3f(const char *name, float a, float b, float c) {
  glUniform3f(glGetUniformLocation(ctx_state()->shader->id, name), a, b, c);
}

void ctx_set_uniform_1i(const char *name, int a) {
  glUniform1i(glGetUniformLocation(ctx_state()->shader->id, name), a);
}

void ctx_set_uniform_2fv(const char *name, size_t cnt, float *values) {
  glUniform2fv(glGetUniformLocation(ctx_state()->shader->id, name), cnt, values);
}

void ctx_set_uniform_1iv(const char *name, size_t cnt, int *values) {
  glUniform1iv(glGetUniformLocation(ctx_state()->shader->id, name), cnt, values);
}



void ctx_viewport(int x, int y, int w, int h) {
  ctx_state()->viewport.x = x;
  ctx_state()->viewport.y = y;
  ctx_state()->viewport.w = w;
  ctx_state()->viewport.h = h;
  glViewport(x, y, w, h);
}
