/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <stdio.h>
#include <time.h>
#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#define CIMGUI_USE_GLFW
#define CIMGUI_USE_OPENGL3
#include <cimgui.h>
#include <cimgui_impl.h>
#include <digitful/gl_utils.h>
#include <digitful/gres.h>
#include <digitful/lights.h>
#include <digitful/renderer.h>
#include <digitful/shader.h>
#include <digitful/utils.h>

Renderer *renderer;
LightMap *one_map;
LightMap *all_map;
GLuint bg;

typedef struct {
  struct {
    bool is_image;
    float r, g, b;
  } bg;

  LightObstacle *obstacles;

  struct {
    bool is_enabled;
    Light light;
  } ptr_light;

  struct {
    int lightc;
    Light lights[100];
    float angle;
    float freq;
    float dist;
  } spin_lights;

  struct {
    bool are_enabled;
    Light lights[4]; // Ordered from bottom to top, left to right
  } corner_lights;
} Scene;
Scene scene;

void delete_all_obstacles() {
  for(size_t i = 0; i < vec_size(scene.obstacles); i++) {
    light_obstacle_deinit(&scene.obstacles[i]);
  }
  vec_clear(scene.obstacles);
}

void add_random_obstacle() {
  float xs[6], ys[6];
  size_t vertexc = rand() % 3 + 4;

  float w = renderer->width * 2.0 / 3.0, h = renderer->height * 2.0 / 3.0;
  xs[0] = fmod(rand(), w) - w / 2;
  ys[0] = fmod(rand(), h) - h / 2;

  bool is_last_seg_bad = true;
  while(is_last_seg_bad) {
    is_last_seg_bad = false;

    for(size_t i = 1; i < vertexc; i++) {
      xs[i] = xs[i - 1] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);
      ys[i] = ys[i - 1] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);

      // This magic equation ensures that the last two line segments are not colinear.
      bool is_bad = i >= 2 && which_side(xs[i], ys[i], xs[i - 1], ys[i - 1], xs[i - 2], ys[i - 2]) == 0;
      for(size_t j = 1; j + 1 < i && !is_bad; j++) {
        is_bad |= do_intersect(xs[i], ys[i], xs[i - 1], ys[i - 1], xs[j], ys[j], xs[j - 1], ys[j - 1]);
      }
      if(is_bad) {
        i--;
      }
    }

    for(size_t i = 1; i + 1 < vertexc - 1 && !is_last_seg_bad; i++) {
      is_last_seg_bad |= do_intersect(xs[0], ys[0], xs[vertexc - 1], ys[vertexc - 1], xs[i], ys[i], xs[i + 1], ys[i + 1]);
    }
  }

  float r = rand() % 256 / 255.0, g = rand() % 256 / 255.0, b = rand() % 256 / 255.0;
  LightObstacle obstacle;
  light_obstacle_init(&obstacle, r, g, b, xs, ys, vertexc, true);
  vec_append(scene.obstacles, obstacle);
}

void reset_spin_colors() {
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    float r = 0.0, g = 0.0, b = 0.0;
    float hue = 6.0 * i / scene.spin_lights.lightc;
    float x = 1.0 - fabs(fmod(hue, 2.0) - 1.0);
    if(hue < 1.0) {
      r = 1.0;
      g = x;
    } else if(hue < 2.0) {
      r = x;
      g = 1.0;
    } else if(hue < 3.0) {
      g = 1.0;
      b = x;
    } else if(hue < 4.0) {
      g = x;
      b = 1.0;
    } else if(hue < 5.0) {
      b = 1.0;
      r = x;
    } else if(hue < 6.0) {
      b = x;
      r = 1.0;
    }
    scene.spin_lights.lights[i].r = r;
    scene.spin_lights.lights[i].g = g;
    scene.spin_lights.lights[i].b = b;
  }
}

void preset_default() {
  puts("Loading preset 'default'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;

  float xs[] = {-1.0,  0.0,  1.0, 1.0, 0.0, -1.0};
  float ys[] = {-2.0, -2.0, -1.0, 2.0, 1.0,  1.0};
  size_t vertexc = array_size(xs);
  for(size_t i = 0; i < vertexc; i++) {
    xs[i] = xs[i] * 15.0;
    ys[i] = ys[i] * 15.0;
  }
  LightObstacle poly1, poly2;
  light_obstacle_init(&poly1, 0.0, 0.0, 0.0, xs, ys, vertexc, false);
  light_obstacle_init(&poly2, 0.0, 0.0, 0.0, xs, ys, vertexc, true);
  light_obstacle_move(&poly1,  80.0, 0.0);
  light_obstacle_move(&poly2, -80.0, 0.0);
  delete_all_obstacles();
  vec_append(scene.obstacles, poly1);
  vec_append(scene.obstacles, poly2);

  scene.ptr_light.is_enabled = false;
  scene.spin_lights.lightc = 6;
  reset_spin_colors();
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i].dist_mul = 1.0;
  }
  scene.spin_lights.freq = 0.25;
  scene.spin_lights.dist = 200.0;
  scene.corner_lights.are_enabled = false;
}

void preset_white_red() {
  puts("Loading preset 'white and red'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  for(size_t i = 0; i < vec_size(scene.obstacles); i++) {
    scene.obstacles[i].r = 1.0;
    scene.obstacles[i].g = 0.0;
    scene.obstacles[i].b = 0.0;
  }
  scene.ptr_light.light.r = 1.0;
  scene.ptr_light.light.g = 1.0;
  scene.ptr_light.light.b = 1.0;
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i].r = 1.0;
    scene.spin_lights.lights[i].g = 1.0;
    scene.spin_lights.lights[i].b = 1.0;
  }
  for(int i = 0; i < 4; i++) {
    scene.corner_lights.lights[i].r = 1.0;
    scene.corner_lights.lights[i].g = 1.0;
    scene.corner_lights.lights[i].b = 1.0;
  }
}

void preset_ptr_black_hole() {
  puts("Loading preset 'pointer black hole'...");
  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = -1.0, .g = -1.0, .b = -1.0,
    .dist_mul = 1.0
  };
}

void preset_colorful() {
  puts("Loading preset 'colorful'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  delete_all_obstacles();
  for(int i = 0; i < 5; i++) {
    add_random_obstacle();
  }
  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 0.05
  };
  scene.spin_lights.lightc = 0;
  scene.corner_lights.are_enabled = false;
}

void preset_glowingly_colorful() {
  puts("Loading preset 'glowingly colorful'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  delete_all_obstacles();
  for(int i = 0; i < 5; i++) {
    add_random_obstacle();
  }
  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 0.05
  };
  scene.spin_lights.lightc = 0;
  scene.corner_lights.are_enabled = true;
  for(int i = 0; i < 4; i++) {
    scene.corner_lights.lights[i] = (Light) {
      .r = 1.0, .g = 1.0, .b = 1.0,
      .dist_mul = 0.25
    };
  }
}

void preset_rgb_basement() {
  puts("Loading preset 'rgb basement'...");
  scene.bg.is_image = true;
  delete_all_obstacles();
  for(int i = 0; i < 8; i++) {
    add_random_obstacle();
    scene.obstacles[i].r = 0.0;
    scene.obstacles[i].g = 0.0;
    scene.obstacles[i].b = 0.0;
  }
  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 1.0
  };
  scene.spin_lights.lightc = 3;
  reset_spin_colors();
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i].dist_mul = 1.0;
  }
  scene.spin_lights.freq = 0.25;
  scene.spin_lights.dist = 200.0;
  scene.corner_lights.are_enabled = true;
  for(int i = 0; i < 4; i++) {
    scene.corner_lights.lights[i] = (Light) {
      .r = 1.0, .g = 1.0, .b = 1.0,
      .dist_mul = 1.0
    };
  }
}

void preset_the_sun() {
  puts("Loading preset 'the sun'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  delete_all_obstacles();
  for(int i = 0; i < 8; i++) {
    add_random_obstacle();
    scene.obstacles[i].r = 0.0;
    scene.obstacles[i].g = 0.0;
    scene.obstacles[i].b = 0.0;
  }
  scene.ptr_light.is_enabled = false;
  scene.spin_lights.lightc = 20;
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i] = (Light) {
      .r = 1.0, .g = 0.5, .b = 0.0,
      .dist_mul = 1.0
    };
  }
  scene.spin_lights.freq = 0.25;
  scene.spin_lights.dist = 50.0;
  scene.corner_lights.are_enabled = false;
}

void preset_quadrilaterals() {
  puts("Loading preset 'quadrilaterals'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  delete_all_obstacles();
  for(int i = 0; i < 50; i++) {
    float xs[2], ys[2];
    // The effectiveness of constants in the following two lines depend on the spinning lights' distance from the center.
    xs[0] = rand() % 501 - 250;
    ys[0] = rand() % 501 - 250;
    xs[1] = xs[0] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);
    ys[1] = ys[0] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);

    float r = rand() % 256 / 255.0, g = rand() % 256 / 255.0, b = rand() % 256 / 255.0;
    LightObstacle obstacle;
    light_obstacle_init(&obstacle, r, g, b, xs, ys, 2, false);
    vec_append(scene.obstacles, obstacle);
  }
  scene.ptr_light.is_enabled = false;
  scene.spin_lights.lightc = 5;
  reset_spin_colors();
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i].dist_mul = 0.1;
  }
  scene.spin_lights.freq = 0.25;
  scene.spin_lights.dist = 200.0;
  scene.corner_lights.are_enabled = false;
}

void preset_the_ring() {
  puts("Loading preset 'the ring'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;
  delete_all_obstacles();
  for(int i = 0; i < 50; i++) {
    float xs[2], ys[2];
    xs[0] = rand() % 351 - 175;
    ys[0] = rand() % 351 - 175;
    xs[1] = xs[0] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);
    ys[1] = ys[0] + (rand() % 51 + 25) * (rand() % 2 * 2 - 1);

    LightObstacle obstacle;
    light_obstacle_init(&obstacle, 0.0, 0.0, 0.0, xs, ys, 2, false);
    vec_append(scene.obstacles, obstacle);
  }
  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 1.0
  };
  scene.spin_lights.lightc = 50;
  reset_spin_colors();
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    scene.spin_lights.lights[i].dist_mul = 1.0;
  }
  scene.spin_lights.freq = 0.25;
  scene.spin_lights.dist = 275.0;
  scene.corner_lights.are_enabled = false;
}

void preset_maze() {
  puts("Loading preset 'maze'...");
  scene.bg.is_image = false;
  scene.bg.r = 0.9;
  scene.bg.g = 0.9;
  scene.bg.b = 0.9;

  delete_all_obstacles();

  int w = min((int) one_map->view_w / 50.0, 256), h = min((int) one_map->view_h / 50.0, 256);
  bool has_vertical_wall[256][255], has_horizontal_wall[255][256];
  memset(has_vertical_wall, 1, sizeof(has_vertical_wall));
  memset(has_horizontal_wall, 1, sizeof(has_horizontal_wall));

  const int dir_x[4] = {1, 0, -1, 0};
  const int dir_y[4] = {0, 1, 0, -1};

  bool is_visited[256][256];
  memset(is_visited, 0, sizeof(is_visited));
  int visitedc = 0;
  is_visited[0][0] = true;
  visitedc++;

  while(visitedc != w * h) {
    int curr_x = -1, curr_y = -1;
    while(curr_x == -1) {
      int x = rand() % w, y = rand() % h;
      if(!is_visited[y][x]) continue;
      for(int i = 0; i < 4 && curr_x == -1; i++) {
        int nx = x + dir_x[i], ny = y + dir_y[i];
        if(nx < 0 || ny < 0 || nx >= w || ny >= h) continue;
        if(is_visited[ny][nx]) continue;
        curr_x = x;
        curr_y = y;
      }
    }

    bool should_continue;
    do {
      should_continue = false;
      int dir = rand() % 4;
      for(int i = 0; i < 4; i++, dir++) {
        int nx = curr_x + dir_x[dir], ny = curr_y + dir_y[dir];
        if(nx < 0 || ny < 0 || nx >= w || ny >= h) continue;
        if(is_visited[ny][nx]) continue;
        should_continue = true;
        break;
      }

      if(should_continue) {
        if(dir_x[dir] == 1) {
          has_vertical_wall[curr_y][curr_x] = false;
        }
        if(dir_x[dir] == -1) {
          has_vertical_wall[curr_y][curr_x - 1] = false;
        }
        if(dir_y[dir] == 1) {
          has_horizontal_wall[curr_y][curr_x] = false;
        }
        if(dir_y[dir] == -1) {
          has_horizontal_wall[curr_y - 1][curr_x] = false;
        }
        curr_x += dir_x[dir];
        curr_y += dir_y[dir];
        is_visited[curr_y][curr_x] = true;
        visitedc++;
      }
    } while(should_continue);
  }

  for(int x = 0; x < w - 1; x++) {
    float xs[2], ys[2];
    xs[0] = xs[1] = (x + 1) * one_map->view_w / w;
    for(int y = 0; y < h; y++) {
      if(!has_vertical_wall[y][x]) continue;
      if(y - 1 < 0 || !has_vertical_wall[y - 1][x]) {
        ys[0] = y * one_map->view_h / h;
      }
      if(y + 1 >= h || !has_vertical_wall[y + 1][x]) {
        ys[1] = (y + 1) * one_map->view_h / h;
        LightObstacle obstacle;
        light_obstacle_init(&obstacle, 0.0, 0.0, 0.0, xs, ys, 2, false);
        light_obstacle_move(&obstacle, one_map->view_x, one_map->view_y);
        vec_append(scene.obstacles, obstacle);
      }
    }
  }
  for(int y = 0; y < h - 1; y++) {
    float xs[2], ys[2];
    ys[0] = ys[1] = (y + 1) * one_map->view_h / h;
    for(int x = 0; x < w; x++) {
      if(!has_horizontal_wall[y][x]) continue;
      if(x - 1 < 0 || !has_horizontal_wall[y][x - 1]) {
        xs[0] = x * one_map->view_w / w;
      }
      if(x + 1 >= w || !has_horizontal_wall[y][x + 1]) {
        xs[1] = (x + 1) * one_map->view_w / w;
        LightObstacle obstacle;
        light_obstacle_init(&obstacle, 0.0, 0.0, 0.0, xs, ys, 2, false);
        light_obstacle_move(&obstacle, one_map->view_x, one_map->view_y);
        vec_append(scene.obstacles, obstacle);
      }
    }
  }

  scene.ptr_light.is_enabled = true;
  scene.ptr_light.light = (Light) {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 1.0
  };
  scene.spin_lights.lightc = 0;
  scene.corner_lights.are_enabled = true;
  for(int i = 0; i < 4; i++) {
    scene.corner_lights.lights[i] = (Light) {
      .r = 1.0, .g = 1.0, .b = 1.0,
      .dist_mul = 1.0
    };
  }
}



bool ui_is_editing_obstacle = false;
float *ui_obstacle_xs = NULL, *ui_obstacle_ys = NULL;

void draw_scene() {
  static float last_draw = 0.0;
  float delta_time = app_time() - last_draw;
  last_draw += delta_time;

  light_map_resize(all_map, renderer->width, renderer->height, true);
  light_map_resize(one_map, renderer->width, renderer->height, true);
  light_map_clear(all_map);

  if(ui_is_editing_obstacle && vec_size(ui_obstacle_xs) >= 2) {
    LightObstacle obstacle = {
      .r = 0.0, .g = 0.0, .b = 0.0,
      .xs = ui_obstacle_xs, .ys = ui_obstacle_ys,
      .vertexc = vec_size(ui_obstacle_xs),
      .is_closed = false
    };
    vec_append(scene.obstacles, obstacle);
  }

  if(scene.ptr_light.is_enabled) {
    scene.ptr_light.light.x = renderer->mousex - renderer->width / 2.0;
    scene.ptr_light.light.y = renderer->height / 2.0 - renderer->mousey;
    light_map_add_light(all_map, one_map, scene.ptr_light.light, scene.obstacles, vec_size(scene.obstacles));
  }

  scene.spin_lights.angle += delta_time * scene.spin_lights.freq * M_PI * 2.0;
  for(int i = 0; i < scene.spin_lights.lightc; i++) {
    float angle = scene.spin_lights.angle + i * M_PI * 2.0 / scene.spin_lights.lightc;
    scene.spin_lights.lights[i].x = cos(angle) * scene.spin_lights.dist;
    scene.spin_lights.lights[i].y = sin(angle) * scene.spin_lights.dist;
    light_map_add_light(all_map, one_map, scene.spin_lights.lights[i], scene.obstacles, vec_size(scene.obstacles));
  }

  if(scene.corner_lights.are_enabled) {
    for(int y = 0; y < 2; y++) {
      for(int x = 0; x < 2; x++) {
        int i = 2 * y + x;
        scene.corner_lights.lights[i].x = renderer->width * x - renderer->width / 2.0;
        scene.corner_lights.lights[i].y = renderer->height * y - renderer->height / 2.0;
        light_map_add_light(all_map, one_map, scene.corner_lights.lights[i], scene.obstacles, vec_size(scene.obstacles));
      }
    }
  }

  if(ui_is_editing_obstacle && vec_size(ui_obstacle_xs) >= 2) {
    vec_pop(scene.obstacles);
  }

  if(scene.bg.is_image) {
    ctx_use_shader(simple_tex_shader);
    glBindTexture(GL_TEXTURE_2D, bg);
    glBindVertexArray(tex_overlay_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
  } else {
    glClearColor(scene.bg.r, scene.bg.g, scene.bg.b, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
  }

  ctx_bind_fbo(0);
  light_map_apply_with(all_map, renderer->offscreen_fbo_tex);
}



int ui_fps = 0;
const ImVec2 zero_imvec2 = {0.0, 0.0};

void ui_light_edit(Light *light) {
  igPushID_Ptr(light);

  float color[3] = {light->r, light->g, light->b};
  igColorEdit3("color", color, ImGuiColorEditFlags_Float);
  light->r = color[0];
  light->g = color[1];
  light->b = color[2];

  igBeginDisabled(light->r == 0.0);
  if(igButton("-R", zero_imvec2)) {
    light->r *= -1.0;
  }
  igEndDisabled();

  igBeginDisabled(light->g == 0.0);
  igSameLine(0.0, igGetStyle()->ItemInnerSpacing.x);
  if(igButton("-G", zero_imvec2)) {
    light->g *= -1.0;
  }
  igEndDisabled();

  igBeginDisabled(light->b == 0.0);
  igSameLine(0.0, igGetStyle()->ItemInnerSpacing.x);
  if(igButton("-B", zero_imvec2)) {
    light->b *= -1.0;
  }
  igEndDisabled();

  igDragFloat("distance multiplier", &light->dist_mul, 0.005, 0.0, 10.0, "%f", 0);

  igPopID();
}

void draw_ui() {
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  igNewFrame();

  static bool is_scene_options_visible = false;
  static bool is_demo_window_visible = false;

  if(renderer->was_esc_pressed) {
    is_scene_options_visible = !is_scene_options_visible;
    renderer->was_esc_pressed = false;
  }
  if(renderer->was_mouse_pressed) {
    if(!igGetIO()->WantCaptureMouse && ui_is_editing_obstacle) {
      vec_append(ui_obstacle_xs, renderer->mousex - renderer->width / 2.0);
      vec_append(ui_obstacle_ys, renderer->height / 2.0 - renderer->mousey);
    }
    renderer->was_mouse_pressed = false;
  }
  if(renderer->was_backspace_pressed) {
    if(!igGetIO()->WantCaptureKeyboard && ui_is_editing_obstacle) {
      if(vec_size(ui_obstacle_xs) > 1) {
        vec_pop(ui_obstacle_xs);
        vec_pop(ui_obstacle_ys);
      } else {
        ui_is_editing_obstacle = false;
        vec_destroy(ui_obstacle_xs);
        vec_destroy(ui_obstacle_ys);
      }
    }
    renderer->was_backspace_pressed = false;
  }

  /*
   * Note: The actual colors of the lights and obstacles don't match up
   * with the ones in the color pickers because of gamma correction.
   */

  if(is_scene_options_visible) {
    igBegin("scene options", &is_scene_options_visible, 0);
    igSetWindowSize_Vec2((ImVec2) {200.0, 300.0}, ImGuiCond_Once);

    if(igCollapsingHeader_TreeNodeFlags("presets", 0)) {
      if(igButton("default", zero_imvec2)) preset_default();
      if(igButton("white and red", zero_imvec2)) preset_white_red();
      if(igButton("pointer black hole", zero_imvec2)) preset_ptr_black_hole();
      if(igButton("colorful", zero_imvec2)) preset_colorful();
      if(igButton("glowingly colorful", zero_imvec2)) preset_glowingly_colorful();
      if(igButton("rgb basement", zero_imvec2)) preset_rgb_basement();
      if(igButton("the sun", zero_imvec2)) preset_the_sun();
      if(igButton("quadrilaterals", zero_imvec2)) preset_quadrilaterals();
      if(igButton("the ring", zero_imvec2)) preset_the_ring();
      if(igButton("maze", zero_imvec2)) preset_maze();
    }

    if(igCollapsingHeader_TreeNodeFlags("background", 0)) {
      igCheckbox("is image", &scene.bg.is_image);
      igBeginDisabled(scene.bg.is_image);
      float color[3] = {scene.bg.r, scene.bg.g, scene.bg.b};
      igColorEdit3("color", color, 0);
      scene.bg.r = color[0];
      scene.bg.g = color[1];
      scene.bg.b = color[2];
      igEndDisabled();
    }

    if(igCollapsingHeader_TreeNodeFlags("obstacles", 0)) {
      if(ui_is_editing_obstacle) {
        if(igButton("stop editing", zero_imvec2)) {
          ui_is_editing_obstacle = false;
          if(vec_size(ui_obstacle_xs) >= 2) {
            LightObstacle obstacle;
            light_obstacle_init(&obstacle, 0.0, 0.0, 0.0, ui_obstacle_xs, ui_obstacle_ys, vec_size(ui_obstacle_xs), false);
            vec_append(scene.obstacles, obstacle);
          }
          vec_destroy(ui_obstacle_xs);
          vec_destroy(ui_obstacle_ys);
        }
        if(igIsItemHovered(0)) {
          igBeginTooltip();
          igSetTooltip("left mouse button - add a new vertex\n"
                       "backspace - remove the last added vertex");
          igEndTooltip();
        }
      } else {
        if(igButton("edit a new one", zero_imvec2)) {
          ui_is_editing_obstacle = true;
          ui_obstacle_xs = vec_new(float);
          ui_obstacle_ys = vec_new(float);
        }
      }
      if(igButton("add a random one", zero_imvec2)) add_random_obstacle();
      if(igButton("delete all", zero_imvec2)) delete_all_obstacles();

      for(size_t i = 0; i < vec_size(scene.obstacles); i++) {
        LightObstacle *obstacle = &scene.obstacles[i];
        if(igTreeNode_Ptr(obstacle, "obstacle %i", i + 1)) {
          float color[3] = {obstacle->r, obstacle->g, obstacle->b};
          igColorEdit3("occlusion", color, ImGuiColorEditFlags_Float);
          obstacle->r = color[0];
          obstacle->g = color[1];
          obstacle->b = color[2];
          igValue_Int("vertex count", obstacle->vertexc);
          igCheckbox("is closed", &obstacle->is_closed);

          if(igButton("delete", zero_imvec2)) {
            light_obstacle_deinit(obstacle);
            memmove(scene.obstacles + i, scene.obstacles + i + 1, sizeof(LightObstacle) * (vec_size(scene.obstacles) - i - 1));
            vec_pop(scene.obstacles);
            i--;
          }

          igTreePop();
        }
      }
    }

    if(igCollapsingHeader_TreeNodeFlags("pointer light", 0)) {
      igCheckbox("is enabled", &scene.ptr_light.is_enabled);
      igBeginDisabled(!scene.ptr_light.is_enabled);
      ui_light_edit(&scene.ptr_light.light);
      igEndDisabled();
    }

    if(igCollapsingHeader_TreeNodeFlags("spinning lights", 0)) {
      igSliderInt("count", &scene.spin_lights.lightc, 0, 100, "%d", 0);
      float angle = fmod(scene.spin_lights.angle / M_PI * 180.0, 360.0);
      if(angle < 0.0) {
        angle += 360.0;
      }
      igDragFloat("angle", &angle, 1.0, 360.0, 360.0, "%f", 0);
      scene.spin_lights.angle = angle / 180.0 * M_PI;
      igDragFloat("frequency", &scene.spin_lights.freq, 0.01, -5.0, 5.0, "%f", 0);
      igDragFloat("distance", &scene.spin_lights.dist, 1.0, 0.0, 1000.0, "%f", 0);
      if(igButton("reset colors", zero_imvec2)) reset_spin_colors();

      for(int i = 0; i < scene.spin_lights.lightc; i++) {
        Light *light = &scene.spin_lights.lights[i];
        if(igTreeNode_Ptr(light, "light %i", i + 1)) {
          ui_light_edit(light);
          igTreePop();
        }
      }
    }

    if(igCollapsingHeader_TreeNodeFlags("corner lights", 0)) {
      igCheckbox("are enabled", &scene.corner_lights.are_enabled);

      igBeginDisabled(!scene.corner_lights.are_enabled);
      const char *labels[] = {
        "bottom left",
        "bottom right",
        "top left",
        "top right"
      };
      for(int y = 0; y < 2; y++) {
        for(int x = 0; x < 2; x++) {
          int i = 2 * y + x;
          if(igTreeNode_Str(labels[i])) {
            ui_light_edit(&scene.corner_lights.lights[i]);
            igTreePop();
          }
        }
      }
      igEndDisabled();
    }
    if(igCollapsingHeader_TreeNodeFlags("tools", 0)) {
      if(igTreeNode_Str("modify all lights")) {
        Light **lights = vec_new(Light*);
        if(scene.ptr_light.is_enabled) {
          vec_append(lights, &scene.ptr_light.light);
        }
        for(int i = 0; i < scene.spin_lights.lightc; i++) {
          vec_append(lights, &scene.spin_lights.lights[i]);
        }
        if(scene.corner_lights.are_enabled) {
          for(int i = 0; i < 4; i++) {
            vec_append(lights, &scene.corner_lights.lights[i]);
          }
        }

        Light model = {0};
        if(vec_size(lights) >= 1) {
          model = *lights[0];
        }
        for(size_t i = 1; i < vec_size(lights); i++) {
          if(model.r != lights[i]->r) {
            model.r = -0.0;
          }
          if(model.g != lights[i]->g) {
            model.g = -0.0;
          }
          if(model.b != lights[i]->b) {
            model.b = -0.0;
          }
          if(model.dist_mul != lights[i]->dist_mul) {
            model.dist_mul = -0.0;
          }
        }

        igBeginDisabled(vec_size(lights) == 0);
        ui_light_edit(&model);
        igEndDisabled();

        for(size_t i = 0; i < vec_size(lights); i++) {
          if(model.r != 0.0 || !signbit(model.r)) {
            lights[i]->r = model.r;
          }
          if(model.g != 0.0 || !signbit(model.g)) {
            lights[i]->g = model.g;
          }
          if(model.b != 0.0 || !signbit(model.b)) {
            lights[i]->b = model.b;
          }
          if(model.dist_mul != 0.0 || !signbit(model.dist_mul)) {
            lights[i]->dist_mul = model.dist_mul;
          }
        }

        igTreePop();
      }

      if(igTreeNode_Str("modify all obstacles")) {
        float r = 0.0, g = 0.0, b = 0.0;
        int is_closed = 0;
        if(vec_size(scene.obstacles) >= 1) {
          r = scene.obstacles[0].r;
          g = scene.obstacles[0].g;
          b = scene.obstacles[0].b;
          is_closed = scene.obstacles[0].is_closed;
        }
        for(size_t i = 1; i < vec_size(scene.obstacles); i++) {
          if(r != scene.obstacles[i].r) {
            r = -0.0;
          }
          if(g != scene.obstacles[i].g) {
            g = -0.0;
          }
          if(b != scene.obstacles[i].b) {
            b = -0.0;
          }
          if(is_closed != (int) scene.obstacles[i].is_closed) {
            is_closed = -1;
          }
        }

        igBeginDisabled(vec_size(scene.obstacles) == 0);
        float color[3] = {r, g, b};
        igColorEdit3("occlusion", color, ImGuiColorEditFlags_Float);
        r = color[0];
        g = color[1];
        b = color[2];
        igPushItemFlag(ImGuiItemFlags_MixedValue, is_closed == -1);
        bool is_closed_bool = is_closed == 1;
        igCheckbox("is closed", &is_closed_bool);
        if(is_closed_bool != (is_closed == 1)) {
          is_closed = is_closed_bool;
        }
        igPopItemFlag();
        if(igButton("delete", zero_imvec2)) {
          delete_all_obstacles();
        }
        igEndDisabled();

        for(size_t i = 0; i < vec_size(scene.obstacles); i++) {
          if(r != 0.0 || !signbit(r)) {
            scene.obstacles[i].r = r;
          }
          if(g != 0.0 || !signbit(g)) {
            scene.obstacles[i].g = g;
          }
          if(b != 0.0 || !signbit(b)) {
            scene.obstacles[i].b = b;
          }
          if(is_closed != -1) {
            scene.obstacles[i].is_closed = is_closed;
          }
        }

        igTreePop();
      }
    }

    if(igCollapsingHeader_TreeNodeFlags("debug", 0)) {
      igValue_Int("number of fps'es", ui_fps);
      igValue_Float("app time", app_time(), "%fs");
      igDragFloat("gamma", &all_map->gamma, 0.01, 0.0, 10.0, "%f", 0);
      igCheckbox("demo window", &is_demo_window_visible);
    }

    igEnd();
  }

  if(is_demo_window_visible) {
    igShowDemoWindow(&is_demo_window_visible);
  }

  igRender();
  ImGui_ImplOpenGL3_RenderDrawData(igGetDrawData());
}



void register_gres();

int main() {
  srand(time(NULL));
  gres_init();
  register_gres();
  ren_init();

  puts("Creating renderer...");
  renderer = renderer_new(800, 600, "digitful");
  if(renderer == NULL) {
    exit(EXIT_FAILURE);
  }
  renderer->has_offscreen_fbo = true;
  renderer->should_blit_offscreen_fbo = false;

  gl_utils_init();
  lights_init();

  puts("Creating light maps...");
  all_map = light_map_new(renderer->width, renderer->height);
  one_map = light_map_new(renderer->width, renderer->height);
  if(all_map == NULL || one_map == NULL) {
    exit(EXIT_FAILURE);
  }

  puts("Loading background image...");
  bg = tex2d_new_from_gres("bg.jpg", GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR);
  if(bg == 0) {
    exit(EXIT_FAILURE);
  }

  puts("Creating scene...");
  const Light zero_light = {
    .r = 1.0, .g = 1.0, .b = 1.0,
    .dist_mul = 1.0
  };
  scene = (Scene) {
    .bg = {
      .is_image = false,
      .r = 1.0, .g = 1.0, .b = 1.0
    },
    .obstacles = vec_new(LightObstacle),
    .ptr_light = {
      .is_enabled = false,
      .light = zero_light
    },
    .spin_lights = {
      .lightc = 0,
      .angle = 0.0,
      .freq = 0.0,
      .dist = 0.0
    },
    .corner_lights = {
      .are_enabled = false,
      .lights = {zero_light, zero_light, zero_light, zero_light}
    }
  };
  for(size_t i = 0; i < array_size(scene.spin_lights.lights); i++) {
    scene.spin_lights.lights[i] = zero_light;
  }
  preset_default();

  puts("Initializing user interface...");
  ImGuiContext *ui_ctx = igCreateContext(NULL);
  ImGui_ImplGlfw_InitForOpenGL(renderer->window, true);
  ImGui_ImplOpenGL3_Init("#version 330 core");
  igGetIO()->IniFilename = NULL;

  igStyleColorsDark(NULL);
  ImGuiStyle *style = igGetStyle();
  style->WindowRounding = 5.0;
  style->WindowBorderSize = 0.0;
  style->WindowTitleAlign.x = 0.5;
  style->WindowMenuButtonPosition = ImGuiDir_None;
  style->PopupRounding = 5.0;
  style->PopupBorderSize = 0.0;
  style->FrameRounding = 1.0;

  Resource *font = gres_get("font.ttf");
  ImFontConfig *font_cfg = ImFontConfig_ImFontConfig();
  font_cfg->FontDataOwnedByAtlas = false;
  font_cfg->OversampleH = 2;
  font_cfg->OversampleV = 2;
  ImFontAtlas_AddFontFromMemoryTTF(igGetIO()->Fonts, font->data, sizeof(unsigned char) * font->size, 16.0, font_cfg, ImFontAtlas_GetGlyphRangesDefault(igGetIO()->Fonts));
  ImFontConfig_destroy(font_cfg);
  gres_release(font);

  puts("Press Esc to toggle the scene options window.");

  int fps = 0;
  float last_fps_reset = app_time();
  while(!renderer->should_close) {
    renderer_begin(renderer);
    draw_scene();
    draw_ui();
    renderer_end(renderer);

    fps++;
    if(app_time() - last_fps_reset >= 1.0) {
      last_fps_reset += 1.0;
      ui_fps = fps;
      fps = 0;
    }
  }

  puts("Deinitializing user interface...");
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  igDestroyContext(ui_ctx);

  puts("Destroying scene...");
  delete_all_obstacles();
  vec_destroy(scene.obstacles);

  puts("Destroying background image...");
  glDeleteTextures(1, &bg);

  puts("Destroying light maps...");
  light_map_destroy(all_map);
  light_map_destroy(one_map);

  lights_deinit();
  gl_utils_deinit();

  puts("Destroying renderer...");
  renderer_destroy(renderer);

  ren_deinit();
  gres_deinit();
}
