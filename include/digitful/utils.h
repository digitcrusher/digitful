/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_UTILS_H
#define DIGITFUL_UTILS_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#ifndef M_PI
# define M_PI 3.14159265358979323846264338327950288419716939937510
#endif

#define max(a, b) \
  ({ \
    __auto_type _a = (a); \
    __auto_type _b = (b); \
    _a > _b ? _a : _b; \
  })
#define min(a, b) \
  ({ \
    __auto_type _a = (a); \
    __auto_type _b = (b); \
    _a < _b ? _a : _b; \
  })

#define array_size(arr) (sizeof(arr) / sizeof((arr)[0]))



typedef struct {
  size_t type_size;
  size_t capacity;
  size_t size;
} VecHeader;

#define vec_header(self)    (((VecHeader*) (self)) - 1)
#define vec_type_size(self) (vec_header(self)->type_size)
#define vec_capacity(self)  (vec_header(self)->capacity)
#define vec_size(self)      (vec_header(self)->size)

__attribute__((unused))
static void* _vec_new(size_t type_size) {
  void *result = (unsigned char*) malloc(sizeof(VecHeader)) + sizeof(VecHeader);
  vec_type_size(result) = type_size;
  vec_capacity(result) = 0;
  vec_size(result) = 0;
  return result;
}
#define vec_new(Type) ((Type*) _vec_new(sizeof(Type)))

#define vec_destroy(self) \
  do { \
    free(vec_header(self)); \
  } while(false)

#define vec_realloc(self) \
  do { \
    size_t size = sizeof(VecHeader) + vec_type_size(self) * vec_capacity(self); \
    (self) = (void*) ((unsigned char*) realloc(vec_header(self), size) + sizeof(VecHeader)); \
  } while(false)

#define vec_first(self) ((self)[0])
#define vec_last(self)  ((self)[vec_size(self) - 1])

#define vec_grow(self, capacity) \
  do { \
    size_t new_cap = (capacity); \
    if(new_cap <= vec_capacity(self)) break; \
    vec_capacity(self) = new_cap; \
    vec_realloc(self); \
  } while(false)

#define vec_shrink(self, capacity) \
  do { \
    size_t new_cap = max(capacity, vec_size(self)); \
    if(new_cap >= vec_capacity(self)) break; \
    vec_capacity(self) = new_cap; \
    vec_realloc(self); \
  } while(false)

#define vec_resize(self, size) \
  do { \
    size_t new_size = (size); \
    vec_grow(self, new_size); \
    if(new_size > vec_size(self)) { \
      unsigned char *array = (void*) (self); \
      unsigned char *start = array + vec_type_size(self) * vec_size(self); \
      unsigned char *end = array + vec_type_size(self) * new_size; \
      memset(start, 0, end - start); \
    } \
    vec_size(self) = new_size; \
  } while(false)

#define vec_clear(self) \
  do { \
    vec_size(self) = 0; \
  } while(false)

#define vec_append(self, val) \
  do { \
    __auto_type _val = (val); \
    size_t size = vec_size(self); \
    vec_resize(self, size + 1); \
    (self)[size] = _val; \
  } while(false)

#define vec_pop(self) \
  do { \
    assert(vec_size(self) > 0); \
    vec_size(self) = vec_size(self) - 1; \
  } while(false)



#define HASHMAP_UNUSED ((void*) -2)

typedef size_t (*HashFunc)(void *val);
typedef bool (*EqualsFunc)(void *a, void *b);
typedef struct {
  size_t elem_size;
  HashFunc hash;
  EqualsFunc equals;
  size_t elemc;

  void **buckets;
  size_t bucketc;
  size_t occupiedc;
  float max_load_factor;
} HashMap;

void hashmap_init(HashMap *self, size_t elem_size, HashFunc hash, EqualsFunc equals);
HashMap* hashmap_new(size_t elem_size, HashFunc hash, EqualsFunc equals);
void hashmap_deinit(HashMap *self);
void hashmap_destroy(HashMap *self);
void* hashmap_get(HashMap *self, void *elem);
void hashmap_set(HashMap *self, void *elem);
bool hashmap_remove(HashMap *self, void *elem);
void hashmap_rehash(HashMap *self, size_t bucketc);
size_t hashmap_lookup(HashMap *self, void *elem, bool should_treat_unused_as_null);



unsigned char* read_file(const char *filename, bool should_append_null, size_t *out_size);
float app_time();
size_t hash_str(const char *str);
float cross(float ax, float ay, float bx, float by);
int sign(float x);
int which_side(float x, float y, float ax, float ay, float bx, float by);
bool do_intersect(float ax1, float ay1, float ax2, float ay2, float bx1, float by1, float bx2, float by2);

#endif
