/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_CONTEXT_H
#define DIGITFUL_CONTEXT_H

#include <stddef.h>
#include <glad/glad.h>
#include <digitful/shader.h>

/*
 * OpenGL cheatsheet
 *
 * All OpenGL objects are represented by a GLuint id.
 *
 * Delete a Tex2D - glDeleteTextures(1, &tex);
 * Bind a Tex2D - glBindTexture(GL_TEXTURE_2D, tex);
 *
 * Create an FBO - glGenFramebuffers(1, &fbo);
 * Destroy an FBO - glDeleteFramebuffers(1, &fbo);
 * Attach a Tex2D to an FBO - glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
 * Check if FBO is ready - glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE
 *
 * Create a VAO - glGenVertexArrays(1, &vao);
 * Delete a VAO - glDeleteVertexArrays(1, &vao);
 * Bind a VAO - glBindVertexArray(vao);
 * Draw a VAO - glDrawArrays(primitive_type, 0, vertexc);
 *
 * Create a VBO - glGenBuffers(1, &vbo);
 * Delete a VBO - glDeleteBuffers(1, &vbo);
 * Bind a VBO - glBindBuffer(GL_ARRAY_BUFFER, vbo);
 * Set VBO data - glBufferData(GL_ARRAY_BUFFER, size_of_data, data, usage);
 */

typedef struct {
  GLuint draw_fbo, read_fbo;
  Shader *shader;
  struct {
    int x, y, w, h;
  } viewport;
} ContextState;

typedef struct {
  ContextState *state_stack;
} Context;

void context_init(Context *self);
void context_deinit(Context *self);

extern Context *curr_ctx;
ContextState *ctx_state();
void ctx_push();
void ctx_pop();

void ctx_bind_fbo(GLuint fbo);
void ctx_bind_draw_fbo(GLuint fbo);
void ctx_bind_read_fbo(GLuint fbo);

void ctx_use_shader(Shader *shader);
void ctx_set_uniform_1f(const char *name, float a);
void ctx_set_uniform_2f(const char *name, float a, float b);
void ctx_set_uniform_3f(const char *name, float a, float b, float c);
void ctx_set_uniform_1i(const char *name, int a);
void ctx_set_uniform_2fv(const char *name, size_t cnt, float *values);
void ctx_set_uniform_1iv(const char *name, size_t cnt, int *values);

void ctx_viewport(int x, int y, int w, int h);

#endif
