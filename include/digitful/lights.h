/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_LIGHTS_H
#define DIGITFUL_LIGHTS_H

#include <stdbool.h>
#include <stddef.h>
#include <glad/glad.h>

void lights_init();
void lights_deinit();

typedef struct {
  float x, y;
  float r, g, b;
  float dist_mul; // The "weakness" of the light
} Light;

typedef struct {
  int width, height;
  float view_x, view_y, view_w, view_h;
  float gamma;
  GLuint fbo, tex;
  GLuint shadow_vbo, shadow_vao, tex_copy;
} LightMap;

typedef struct {
  float r, g, b;
  float *xs, *ys;
  size_t vertexc;
  bool is_closed;
} LightObstacle;

void light_obstacle_init(LightObstacle *self, float r, float g, float b, float xs[], float ys[], size_t vertexc, bool is_closed);
LightObstacle* light_obstacle_new(float r, float g, float b, float xs[], float ys[], size_t vertexc, bool is_closed);
void light_obstacle_deinit(LightObstacle *self);
void light_obstacle_destroy(LightObstacle *self);
LightObstacle* light_obstacle_copy(LightObstacle *self);
void light_obstacle_move(LightObstacle *self, float offset_x, float offset_y);

bool light_map_init(LightMap *self, int width, int height);
LightMap* light_map_new(int width, int height);
void light_map_deinit(LightMap *self);
void light_map_destroy(LightMap *self);
void light_map_center_view(LightMap *self, float x, float y, float w, float h);
bool light_map_resize(LightMap *self, int width, int height, bool should_center_view);
void light_map_clear(LightMap *self);
void light_map_add(LightMap *self, LightMap *other);
void light_map_apply_with(LightMap *self, GLuint modulated_tex);
void light_map_cast_light(LightMap *self, Light light);
void light_map_cast_shadows(LightMap *self, Light light, LightObstacle obstacles[], size_t obstaclec);
void light_map_add_light(LightMap *self, LightMap *aux, Light light, LightObstacle obstacles[], size_t obstaclec);

#endif
