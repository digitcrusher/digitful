/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_SHADER_H
#define DIGITFUL_SHADER_H

#include <stdbool.h>
#include <glad/glad.h>

typedef struct {
  GLuint id;
} ShaderStage;

bool shader_stage_init_from_mem(ShaderStage *self, const char *source, GLenum type);
bool shader_stage_init_from_file(ShaderStage *self, const char *filename, GLenum type);
bool shader_stage_init_from_gres(ShaderStage *self, const char *name, GLenum type);
ShaderStage* shader_stage_new_from_mem(const char *source, GLenum type);
ShaderStage* shader_stage_new_from_file(const char *filename, GLenum type);
ShaderStage* shader_stage_new_from_gres(const char *name, GLenum type);
void shader_stage_deinit(ShaderStage *self);
void shader_stage_destroy(ShaderStage *self);

typedef struct {
  GLuint id;
} Shader;

void shader_init(Shader *self);
bool shader_init_from_mem(Shader *self, const char *vert_source, const char *frag_source);
bool shader_init_from_files(Shader *self, const char *vert_filename, const char *frag_filename);
bool shader_init_from_gres(Shader *self, const char *vert_name, const char *frag_name);
Shader* shader_new();
Shader* shader_new_from_mem(const char *vert_source, const char *frag_source);
Shader* shader_new_from_files(const char *vert_filename, const char *frag_filename);
Shader* shader_new_from_gres(const char *vert_name, const char *frag_name);
void shader_deinit(Shader *self);
void shader_destroy(Shader *self);
void shader_attach(Shader *self, ShaderStage *stage);
bool shader_link(Shader *self);

#endif
