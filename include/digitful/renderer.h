/*
 * digitful - A 2D lighting simulation in OpenGL
 * Copyright (C) 2020-2022 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DIGITFUL_RENDERER_H
#define DIGITFUL_RENDERER_H

#include <stdbool.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <digitful/context.h>

void ren_init();
void ren_deinit();

typedef struct {
  GLFWwindow *window;
  int width, height;

  bool should_close;
  bool was_resized;
  int mousex, mousey;
  bool was_esc_pressed;
  bool was_mouse_pressed;
  bool was_backspace_pressed;

  Context ctx;

  bool has_offscreen_fbo;
  bool should_blit_offscreen_fbo;
  GLuint offscreen_fbo;
  GLuint offscreen_fbo_tex;
  bool should_refresh_offscreen_fbo;
} Renderer;

bool renderer_init(Renderer *self, int width, int height, const char *title);
Renderer* renderer_new(int width, int height, const char *title);
void renderer_deinit(Renderer *self);
void renderer_destroy(Renderer *self);
bool renderer_begin(Renderer *self);
void renderer_end(Renderer *self);
void renderer_make_ctx_curr(Renderer *self);

#endif
